# TMX SAX parser

Parse TMX file and stream tags to output function. Tags are streamed one-by-one and
if output function encounters 'TmxError', it should remove already stored tags,
i.e. rollback, if db, or delete file, etc.


