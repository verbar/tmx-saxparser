-- | Module      : Imports
--   Description : Common imports
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Imports
  ( module X
  , tryError

  , fst3, snd3, thd3
  )
where

import ClassyPrelude             as X hiding (catch, catchIO, catchJust, catches, handle,
                                       mapM_, replicateM, try, tryIO)
import Prelude                   as X (Read (..))

import Control.Error             as X (failWith, hoistEither, hush, note, (??))
import Control.Monad.Error.Class as X
import Data.Data                 as X hiding (DataType)
import Data.Default              as X
import Data.Foldable             as X (foldrM, mapM_)
import Data.List.NonEmpty        as X (NonEmpty (..), nonEmpty)
import Data.Store                as X
import Data.Time.Clock           as X

import Debug                     as X

------------------------------------------------------------------------------------------
-- Common functions
------------------------------------------------------------------------------------------
{-# INLINE fst3 #-}
fst3 :: (a,b,c) -> a
fst3 (x,_,_) = x

{-# INLINE snd3 #-}
snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x

{-# INLINE thd3 #-}
thd3 :: (a,b,c) -> c
thd3 (_,_,x) = x


tryError :: MonadError e m => m a -> m (Either e a)
tryError m = (Right <$> m) `catchError` (return . Left)
