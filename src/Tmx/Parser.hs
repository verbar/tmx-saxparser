-- | Module      : Tmx.Parser
--   Description : Parsers and checkers for each tag of TMX spec
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Tmx.Parser
  ( -- * TMX lazy streaming parser with output

  -- $generalInfo

    -- ** Tags
    module XT
    -- ** Attributes
  , module XA
    -- ** Errors
  , module XE

    -- ** Types & datatypes
  , TmxState
  , TmxUnknown (..)

  -- * Streaming output functions
  , streamTmxTo
  , streamTmxUnkTo

  -- * Converting translation unit variants
  , toHtml
  , replaceTags
  )
where

import Imports

-- import qualified Data.List.NonEmpty      as NE

import Text.XML.SAXCombinators
    (IdxNode, SaxCombinatorsBackend, processEvents, processNodes, toIdxEventL)
import Tmx.Errors.Errors       as XE
import Tmx.Parser.Attributes   as XA hiding (parseBCP47, renderLang)
import Tmx.Parser.Content      (replaceTags, toHtml)
import Tmx.Parser.Tags         (TmxState, parseTmx, semanticTu, semanticUde)
import Tmx.Types               as XT hiding (TmxParser)


{- $generalInfo
Parse TMX file and stream tags to output function. Tags are streamed one-by-one and
if output function encounters 'TmxError', it should remove already stored tags,
i.e. rollback, if db, or delete file, etc.
-}

-- | Record holding a TMX tag and unknown/unparsed tags found in it.
data TmxUnknown = TmxUnknown
  { tmxTag       :: Tmx       -- ^ TMX core tag
  , unknownNodes :: [IdxNode] -- ^ unknown nodes/tags
  } deriving (Show, Eq)

-- | Parse TMX file and stream it one-by-one tag to output function
streamTmxTo :: (Default a, SaxCombinatorsBackend event location, MonadIO m)
            => (TmxState a -> (Tmx, [IdxNode]) -> m (Either PError (TmxState a)))
            -- ^ output function
            -> Text
            -- ^ title of TMX file
            -> UniqTmxId
            -- ^ unique identifier
            -> [(event, location)]
            -- ^ lazy list of events with their location in source file
            -> m (Either PError (TmxState a))
streamTmxTo output title uId =
  -- send data to some storage, one-by-one
  uncurry (doOutput output)
    -- check semantics of tags data
    . semanticCheck
    -- parse TMX with only the known tags, defined by standard
    . processEvents (parseTmx title uId) (defGlb, def)
    -- convert events to 'IdxNode' and tuple with ordinal
    . zipWith (curry toIdxEventL) [1..]

semanticCheck :: ([(Tmx, [IdxNode])], TmxState a) -> ([(Tmx, [IdxNode])], TmxState a)
semanticCheck = processNodes parseX
  where
    parseX (n, unks, s) = (, unks, s) <$> join (tryError $ go s n)
    go :: (Globals, s) -> Tmx -> Either PError Tmx
    go _ tag = do
      case tag of
        TmxTransUnit tu -> void $ semanticTu tu
        TmxTmx t        -> maybe (Right ()) (\u -> semanticUde u >> pure ()) . hdrUde $ tmxHeader t
        _               -> Right ()
      Right tag

-- | Parse TMX file with unknown/unparsed events and stream it one-by-one tag to output
--   function
streamTmxUnkTo :: (Default a, SaxCombinatorsBackend event location, MonadIO m)
               => (TmxState a -> (TmxUnknown,[IdxNode]) -> m (Either PError (TmxState a)))
               -- ^ output function
               -> Text
               -- ^ title of TMX file
               -> UniqTmxId
               -- ^ unique identifier
               -> [(event, location)]
               -- ^ lazy list of events with their location in source file
               -> m (Either PError (TmxState a))
streamTmxUnkTo output title uId =
  -- send data to some storage, one-by-one
  uncurry (doOutput output)
    -- there are some unknown tags, so let's parse them as well
    . parseUnknownTags
    -- check semantics of tags data
    . semanticCheck
    -- parse TMX with only the known tags, defined by standard
    . processEvents (parseTmx title uId) (defGlb, def)
    -- convert events to 'IdxNode' and tuple with ordinal
    . zipWith (curry toIdxEventL) [1..]


-- | Parser for unknown tags. Simply stores tags/nodes in list
parseUnknownTags :: ([(Tmx, [IdxNode])], TmxState a)
                 -> ([(TmxUnknown, [IdxNode])], TmxState a)
parseUnknownTags = processNodes parseX
  where
    parseX (n, unks, s) = (\(unk,rest) -> (unk, rest, s)) <$> join (tryError $ go s)
      where
        go :: (Globals, s) -> Either PError (TmxUnknown, [IdxNode])
        go _ = Right $ if null unks
                        then (TmxUnknown n [], unks)
                        else (TmxUnknown n unks, [])

-- | Lazily output items
doOutput :: Monad m => (TmxState a -> (t, [IdxNode]) -> m (Either PError (TmxState a)))
                    -> [(t, [IdxNode])]
                    -> TmxState a
                    -> m (Either PError (TmxState a))
doOutput _      []     s = return $ Right s
doOutput output (t:ts) s = either (return . Left) (doOutput output ts) =<< output s t

-- | Initial "empty" Globals value
defGlb :: Globals
defGlb = Globals
  { glbSrcLang    = Left AllLangs
  , glbChngInfo   = emptyChangeInfo
  , glbDataType   = DTUnknown
  , glbSegType    = STSent
  , glbOrigFormat = ""
  , glbOEncoding  = utf16
  }
  where
    emptyChangeInfo = ChangeInfo
      { ciChanged       = mzero
      , ciChangedBy     = mzero
      , ciCreated       = mzero
      , ciCreatedBy     = mzero
      , ciLastUsed      = mzero
      , ciCreateTool    = mzero
      , ciCreateToolVer = mzero
      }
