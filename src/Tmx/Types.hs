-- | Module      : Tmx.Types
--   Description : Datatypes used in parsing TMX files
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
{-# LANGUAGE DeriveAnyClass #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tmx.Types
  (
     TmxParser

    , Globals (..)
    , ChangeInfo (..)

    -- ** TMX core tags and sum type
    , Tmx  (..)
    , TmxCore  (..)
    , HeaderCore (..)
    , NoteCore  (..)
    , PropCore (..)
    , UdeCore (..)
    , MapCore (..)
    , TransUnitCore (..)
    , TransUnitVarCore  (..)

    -- ** TU content datatypes
    , Content
    , ContentTag (..)
    , Bpt  (..)
    , Ept (..)
    , It (..)
    , Ph (..)
    , Hi (..)
    , Sub (..)

    -- ** Aliases
    , utf16
  )
where

import Imports

import Data.Aeson              (FromJSON (..), ToJSON (..))


import Text.XML.SAXCombinators (EventL)
import Tmx.Errors.Errors       (PError)
import Tmx.Parser.Attributes

utf16 :: Text
utf16 = "UTF-16"

-- | Tag parser monad
type TmxParser = MonadError PError

-- | Datatype holding global variables used when not specified in attributes
data Globals = Globals
  { glbSrcLang    :: TmxLang
  , glbChngInfo   :: ChangeInfo
  , glbDataType   :: DataType
  , glbSegType    :: SegType
  , glbOrigFormat :: Text
  , glbOEncoding  :: CharSet
  } deriving (Show, Eq, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

-- | Info about editing TU, who, when, where, what. Used is several tags
data ChangeInfo = ChangeInfo
  { ciChanged       :: Maybe UTCTime
  , ciChangedBy     :: Maybe Text
  , ciCreated       :: Maybe UTCTime
  , ciCreatedBy     :: Maybe Text
  , ciLastUsed      :: Maybe UTCTime
  , ciCreateTool    :: Maybe Text
  , ciCreateToolVer :: Maybe Text
  } deriving (Show, Eq, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

-- | Sum type holding all TMX datatypes
data Tmx =
      TmxDecl         Text
    | TmxTmx          TmxCore
    | TmxTransUnit    TransUnitCore
    | TmxUnknownEvent EventL
    | TmxError        PError
    deriving (Eq, Show, Generic)
instance Store Tmx

-- | Tmx core tag
data TmxCore = TmxCore
  { tmxTitle   :: Text
  , tmxUniqId  :: UniqTmxId
  , tmxVersion :: Text
  , tmxHeader  :: HeaderCore
  } deriving (Show, Eq, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

-- | Header core tag
data HeaderCore = HeaderCore
  { hdrSrcLang    :: TmxLang
  , hdrChngInfo   :: ChangeInfo
  , hdrDataType   :: DataType
  , hdrSegType    :: SegType
  , hdrOrigFormat :: Text
  , hdrAdminLang  :: Lang
  , hdrOEncoding  :: IsDef CharSet
  , hdrProps      :: [PropCore]
  , hdrNotes      :: [NoteCore]
  , hdrUde        :: Maybe UdeCore
  } deriving (Show, Eq, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

-- | Note core tag. Used in several tags
data NoteCore = NoteCore
  { noteOrder     :: Int
  , noteXmlLang   :: IsDef TmxLang
  , noteOEncoding :: IsDef CharSet
  , noteText      :: Text
  } deriving (Show, Eq, Generic, Typeable, Data, Store, Hashable)
    deriving anyclass (NFData)

-- | Prop core tag. Used in several tags
data PropCore = PropCore
  { propType      :: PropType
  , propXmlLang   :: IsDef TmxLang
  , propOEncoding :: IsDef CharSet
  , propText      :: Text
  } deriving (Show, Eq, Generic, Typeable, Data, Store, Hashable)
    deriving anyclass (NFData)

-- | Ude core tag
data UdeCore = UdeCore
  { udeName :: Text
  , udeBase :: Maybe CharSet
  , udeMaps :: NonEmpty MapCore
  } deriving (Show, Eq, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

-- | Map core tag
data MapCore = MapCore
  { mapUnicode :: Int       -- #xF8FF
  , mapCode    :: Maybe Int -- #xF0
  , mapEnt     :: Maybe Text
  , mapSubst   :: Maybe Text
  } deriving (Show, Eq, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

-- | TransUnit core tag
data TransUnitCore = TransUnitCore
  { tuId         :: IsDef TransUnitID
  , tuOrder      :: Int
  , tuSrcLang    :: IsDef TmxLang
  , tuChngInfo   :: ChangeInfo
  , tuDataType   :: IsDef DataType
  , tuSegType    :: IsDef SegType
  , tuOrigFormat :: IsDef Text
  , tuOEncoding  :: IsDef CharSet
  , tuUsageCount :: Int
  , tuProps      :: [PropCore]
  , tuNotes      :: [NoteCore]
  , tuTUVs       :: NonEmpty TransUnitVarCore
  } deriving (Show, Eq, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

-- | TransUnitVariant core tag
data TransUnitVarCore = TransUnitVarCore
  { tuvXmlLang    :: TmxLang
  , tuvChngInfo   :: ChangeInfo
  , tuvDataType   :: IsDef DataType
  , tuvOrigFormat :: IsDef Text
  , tuvOEncoding  :: IsDef CharSet
  , tuvUsageCount :: Int
  , tuvProps      :: [PropCore]
  , tuvNotes      :: [NoteCore]
  , tuvContent    :: Content
  } deriving (Show, Eq, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

-- | Content alias holding ordinal and a ContentTag
type Content  = [(Int,ContentTag)]

-- | Content sum type
data ContentTag = BPT  Bpt
                | EPT  Ept
                | IT   It
                | PH   Ph
                | HI   Hi
                | SUB  Sub
                | TEXT Text
                | COMM Text
                deriving (Eq, Show, Generic, Typeable, Data, Store, ToJSON, FromJSON)
                deriving anyclass (NFData)

-- | Bpt tag
data Bpt = Bpt
  { bptIdx      :: Int
  , bptExternal :: Maybe Int
  , bptType     :: Maybe PropType
  , bptContent  :: Content
  } deriving (Eq, Show, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)
deriving instance (ToJSON   Bpt)
deriving instance (FromJSON Bpt)

-- | Ept tag
data Ept = Ept
  { eptIdx      :: Int
  , eptExternal :: Maybe Int
  , eptContent  :: Content
  } deriving (Eq, Show, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)
deriving instance (ToJSON   Ept)
deriving instance (FromJSON Ept)

-- | It tag
data It = It
  { itBeginning :: Bool
  , itExternal  :: Maybe Int
  , itType      :: Maybe PropType
  , itContent   :: Content
  } deriving (Eq, Show, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)
deriving instance (ToJSON   It)
deriving instance (FromJSON It)

-- | Ph tag
data Ph = Ph
  { phExternal :: Maybe Int
  , phType     :: Maybe PropType
  , phAssoc    :: Maybe PhAssoc
  , phContent  :: Content
  } deriving (Eq, Show, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)
deriving instance (ToJSON   Ph)
deriving instance (FromJSON Ph)

-- | Hi tag
data Hi = Hi
  { hiExternal :: Maybe Int
  , hiType     :: Maybe PropType
  , hiContent  :: Content
  } deriving (Eq, Show, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)
deriving instance (ToJSON   Hi)
deriving instance (FromJSON Hi)

-- | Sub tag
data Sub = Sub
  { subDataType :: Maybe DataType
  , subType     :: Maybe PropType
  , subContent  :: Content
  } deriving (Eq, Show, Generic, Typeable, Data, Store)
    deriving anyclass (NFData)

deriving instance (ToJSON   Sub)
deriving instance (FromJSON Sub)
