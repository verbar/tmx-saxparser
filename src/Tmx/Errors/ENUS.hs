-- | Module      : Tmx.Errors.ENUS
--   Description : Error descriptions for US English
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Tmx.Errors.ENUS (ENUS(..)) where

import Imports


import Tmx.Errors.Errors  (ShowPError (..), PError (..))
import Text.XML.SAXCombinators (ShowParseLocation(..))


showErrLoc :: ShowParseLocation loc => Either Text loc -> Text
showErrLoc (Left  t)                    = "@ " <> t
showErrLoc (Right loc) = showParseLocation loc

-- | Datatype for specifying ENUS language
data ENUS = ENUS

instance ShowPError PError ENUS where
  showPError _ (CustomError t loc) = "Custom: " <> t  <> showErrLoc loc
  showPError _ (ParseError e) = "Backend parser: " <> tshow e
  showPError _ (StoreError e) = "streamTmxTo function: " <> e
  showPError _ (MustHaveOne (tag,subTag) loc) =
    "<" <> tag <> "> must have at least one " <> subTag <> " element"
    <> showErrLoc loc
  showPError _ (MissingTag (par,tag) loc) =
    "<" <> par <> "> must have <" <> tag <> "> tag" <> showErrLoc loc
  showPError _ (ExtraTag (par,tag) loc) =
    "In <" <> par <> "> tag only [" <> tag <> "] tags are allowed" <> showErrLoc loc
  showPError _ (WrongTag ts loc) = "Not a <" <> ts <> "> tag" <> showErrLoc loc
  showPError _ (MissingAttr (attr,tag) loc) =
    "'" <> attr <> "' attribute must be specified in <" <> tag <> "> tag"
    <> showErrLoc loc
  showPError _ (InvalidValue (attr,val) loc) =
    "Attribute '" <> attr <> "' has illegal value: '" <> val <> "'" <> showErrLoc loc
  showPError _ (BadVersion (bad,supported) loc) =
    "Version '" <> bad <> "' not supported. " <>
    "Only version(s) '" <> supported <> "'."  <> showErrLoc loc
  showPError _ (UnknownElement t loc) = "Unknown element: " <> t <> showErrLoc loc
  showPError _ (IfCodeMapUdeBase loc) =
    "<ude> must have 'base' set if 'code' is used in <map>" <> showErrLoc loc
  showPError _ (EptBeforeBpt loc) =
    "<ept> must be after its <bpt> tag" <> showErrLoc loc
  showPError _ (InlCodeMissingInVariant tag loc) =
    "Inline code <" <> tag <> "> from source missing in target" <> showErrLoc loc
