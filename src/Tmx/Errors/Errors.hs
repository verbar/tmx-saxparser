-- | Module      : Tmx.Errors
--   Description : Possible errors in parsing TMX
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Tmx.Errors.Errors
  ( PError (..)
  , ShowPError (..)
  )
where

import Imports
import Text.XML.SAXCombinators (ParseLocation)


-- | Implement to show 'PError's in any language
class ShowPError a l where showPError :: l -> a -> Text

-- | Errors returned from parser
data PError =
    CustomError             Text       (Either Text ParseLocation)
  -- ^ Custom error
  | ParseError              Text
  -- ^ Backend parser error
  | StoreError              Text
  -- ^   showPError _ (StoreError e) = "streamTmxTo function: " <> e
  | MustHaveOne            (Text,Text) (Either Text ParseLocation)
  -- ^ Parent tag must have at least one of specified tags
  | MissingTag             (Text,Text) (Either Text ParseLocation)
  -- ^ A required tag is missing
  | ExtraTag               (Text,Text) (Either Text ParseLocation)
  -- ^ An extra tag appeared where it is not allowed
  | WrongTag                Text       (Either Text ParseLocation)
  -- ^ Wrong tag send to processing to wrong function. Should never happen
  | MissingAttr            (Text,Text) (Either Text ParseLocation)
  -- ^ A required attribute is missing in tag
  | InvalidValue           (Text,Text) (Either Text ParseLocation)
  -- ^ Attribute has invalid value
  | BadVersion             (Text,Text) (Either Text ParseLocation)
  -- ^ TMX version is not supported
  | UnknownElement          Text       (Either Text ParseLocation)
  -- ^ Some tags do not allow unknown elements as children
  | IfCodeMapUdeBase                   (Either Text ParseLocation)
  -- ^ \<ude> tag 'base' set if 'code' is used in \<map>
  | EptBeforeBpt                       (Either Text ParseLocation)
  -- ^ \<ept> tag must appear after its \<bpt> tag
  | InlCodeMissingInVariant Text       (Either Text ParseLocation)
  -- ^  Inline code from source missing in target
  deriving (Eq, Show, Typeable, Generic)
instance Exception PError
instance Store     PError

instance IsString PError where
  fromString s = CustomError (pack s) (Left "No location")
