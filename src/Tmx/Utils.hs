-- | Module      : Tmx.Utils
--   Description : Utilities and helpers used in parsing TMX
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Tmx.Utils where

import Imports

import Numeric                 (readHex)

import Text.XML.SAXCombinators
import Tmx.Errors.Errors
import Tmx.Parser.Attributes
import Tmx.Types

parseTagMay :: TmxParser m => Text -> (IdxNode -> m a) -> [IdxNode] -> m (Maybe a)
parseTagMay t f cs = f `traverse` find (isNamed t . snd) cs

parseTag :: TmxParser m => IdxNode -> Text -> (IdxNode -> m a) -> [IdxNode] -> m a
parseTag n t f cs =
  maybe (mkErr n (MissingTag (getName $ snd n,t))) pure =<< parseTagMay t f cs

reqAttr :: TmxParser m => IdxNode -> Text -> Attrs -> m Text
reqAttr n a as = maybe (mkErr n $ MissingAttr (a,tshow n)) pure $ lookup a as

reqAttrRead :: (TmxParser m, ReadAttr a) => IdxNode -> Text -> Attrs -> m a
reqAttrRead n a as = case lookup a as of
  Nothing -> mkErr n (MissingAttr (a, getName $ snd n))
  Just v  -> maybe (mkErr n (InvalidValue (a,v))) pure $ readAttr v

optAttr :: TmxParser m => Text -> Attrs -> m (Maybe Text)
optAttr a = pure . lookup a

optAttrRead :: (TmxParser m, ReadAttr a) => IdxNode -> Text -> Attrs -> m (Maybe a)
optAttrRead n a as = case lookup a as of
  Nothing -> pure Nothing
  Just v  -> maybe (mkErr n (InvalidValue (a,v))) (pure . Just) $ readAttr v

defAttr :: (Eq a, TmxParser m, ReadAttr a)
        => IdxNode -> a -> Text -> Attrs -> m (a, Bool)
defAttr n defV a as =
  (\case Nothing -> (defV,True)
         Just v  -> (v,) $ v == defV
  ) <$> optAttrRead n a as

parseChangeInfo :: TmxParser m => IdxNode -> Attrs -> m ChangeInfo
parseChangeInfo n as = do
  ciCreateTool    <- optAttr       "creationtool"        as
  ciCreateToolVer <- optAttr       "creationtoolversion" as
  ciCreated       <- optAttrRead n "creationdate"        as
  ciCreatedBy     <- optAttr       "creationid"          as
  ciChanged       <- optAttrRead n "changedate"          as
  ciChangedBy     <- optAttr       "changeid"            as
  ciLastUsed      <- optAttrRead n "lastusagedate"       as
  return $ ChangeInfo {..}

optHexToInt :: TmxParser m => IdxNode -> Maybe Text -> m (Maybe Int)
optHexToInt n (Just t) = Just <$> hexToInt n t
optHexToInt _ Nothing  = pure Nothing

hexToInt :: TmxParser m => IdxNode -> Text -> m Int
hexToInt n t =
  if "#x" `isPrefixOf` t
    then case readHex . drop 2 $ unpack t of
           [(i,"")] -> pure i
           _        -> err
    else err
  where err = mkErr n $ InvalidValue ("hex",t)

getErrLoc :: IdxNode -> Either Text ParseLocation
getErrLoc (_,NElement _ _ l _)     = Right l
getErrLoc (_,NText _)              = Left "Text"
getErrLoc (_,NCData _)             = Left "CData"
getErrLoc (_,NComment _)           = Left "Comment"
getErrLoc (_,NProcInstruction _ _) = Left "Processing Instructions"
getErrLoc (_,NFail _ l)            = Right l

mkErr :: MonadError err m => IdxNode -> (Either Text ParseLocation -> err) -> m a
mkErr e err = throwError . err $ getErrLoc e

semanticErr :: (MonadError e m, Show l) => l -> (Either Text b -> e) -> m a
semanticErr l e = throwError $ e (Left $ tshow l)
