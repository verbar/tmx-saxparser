-- | Module      : Tmx.Parser.Attributes
--   Description : XML attributes in TMX
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
{-# LANGUAGE DeriveAnyClass #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tmx.Parser.Attributes
  ( ReadAttr (..)
  , ShowAttr (..)

  , SegType (..)
  , DataType (..)
  , AllLangs (..)
  , PropType (..)
  , PhAssoc (..)

  , AnnType (..)
  , AppliesTo (..)
  , ElementType (..)
  , Reorder (..)
  , SegState (..)
  , SubType (..)
  , TextDir (..)

  , CharSet
  , TmxLang
  , TransUnitID
  , UniqTmxId
  , IsDef

  , module X
  )
where

import Imports

import Data.Aeson   (FromJSON (..), ToJSON (..))

import Import.BCP47 as X

utcFormatString :: String
utcFormatString = "%Y%m%dT%H%M%SZ"

-- | Convert type with given function from one form to another.
--   Usually from String/Text to Type
convertType :: (Eq s, Bounded a, Enum a) => (a -> s) -> s -> Maybe a
convertType f s = lookup s $ map (f &&& id) [minBound..maxBound]


------------------------------------------------------------------------------------------
--  Classes
------------------------------------------------------------------------------------------
-- | Enables reading various attributes.
--   @readAttr . showAttr@ isn't necessarily id
class ReadAttr a where readAttr :: Text -> Maybe a
-- | Enables showing various attributes
--   @showAttr . readAttr@ isn't necessarily id
class ShowAttr a where showAttr :: a    -> Text

------------------------------------------------------------------------------------------
--  Orphan instances
------------------------------------------------------------------------------------------

instance ShowAttr Bool where showAttr = bool "no" "yes"
instance ReadAttr Bool where readAttr = convertType showAttr

instance ShowAttr Int  where showAttr = tshow
instance ReadAttr Int  where readAttr = readMay

instance ShowAttr Text where showAttr = id
instance ReadAttr Text where
  readAttr "" = Nothing
  readAttr t  = Just t

instance ShowAttr Lang where showAttr = pack . renderLang
instance ReadAttr Lang where readAttr = hush . parseBCP47 . unpack

deriving instance Hashable Lang
deriving instance Typeable Lang
deriving instance Generic Lang
deriving instance Data Lang
deriving instance Read Lang
deriving instance NFData Lang
deriving instance Store Lang

instance ReadAttr UTCTime  where
  readAttr = parseTimeM True defaultTimeLocale utcFormatString . unpack
instance ShowAttr UTCTime  where
  showAttr = pack . formatTime defaultTimeLocale utcFormatString

------------------------------------------------------------------------------------------
--  Datatypes & Types
------------------------------------------------------------------------------------------

-- | Attribute with default value
type IsDef a = (a,Bool)

-- | Character set used, usually "UTF8"
type CharSet = Text

-- | Translation unit ID type
type TransUnitID = Text

-- | Unique identifier for TMX tag
type UniqTmxId = Int

-- | Language attribute, a language or 'Alllangs'
type TmxLang = Either AllLangs Lang

-- | Datatype for @*all*@ languages
data AllLangs = AllLangs
  deriving (Show, Read, Eq, Generic, Typeable, Data, Store, Hashable, ToJSON, FromJSON)
  deriving anyclass (NFData)
instance ShowAttr AllLangs where showAttr _ = "*all*"


-- | Typed values

-- | Datatype for segtype attribute
data SegType =
    STBlock
  | STPara
  | STSent
  | STPhrase
  deriving (Show, Eq, Generic, Typeable, Data, Store, ToJSON, FromJSON, Bounded, Enum)
  deriving anyclass (NFData)

-- | Datatype for datatype attribute
data DataType =
    DTUnknown
  | DTAlpText
  | DTCdf
  | DTCmx
  | DTCpp
  | DTHpTag
  | DTHtml
  | DTInterleaf
  | DTIpf
  | DTJava
  | DTJavascript
  | DTLisp
  | DTMif
  | DTOpenTag
  | DTPascal
  | DTPlainText
  | DTPm
  | DTRtf
  | DTSgml
  | DTStfF
  | DTStfI
  | DTTransit
  | DTVbSript
  | DTWinRes
  | DTXml
  | DTXpTag
  | DTText
  | DTCustom Text
  deriving (Show, Eq, Generic, Typeable, Data, Store, ToJSON, FromJSON)
  deriving anyclass (NFData)

-- | Datatype for proptype attribute
data PropType =
  -- bpt & it
    PTBold
  | PTColor
  | PTDuLined
  | PTFont
  | PTItalic
  | PTLink
  | PTScap
  | PTStruct
  | PTULined
    -- ph
  | PTIndex
  | PTDate
  | PTTime
  | PTFNote
  | PTENote
  | PTAlt
  | PTImage
  | PTPb
  | PTLb
  | PTCb
  | PTInSet
  | PTCustom Text
  deriving (Show, Eq, Generic, Typeable, Data, Store, Hashable, ToJSON, FromJSON)
  deriving anyclass (NFData)

-- | Datatype for phassoc attribute
data PhAssoc = PHAPrev
             | PHAFollowing
             | PHABoth
             deriving (Eq, Show, Generic, Typeable, Data, Store, ToJSON, FromJSON
                      , Bounded, Enum)
             deriving anyclass (NFData)

-- | Datatype for annotation type attribute
data AnnType = AtGeneric
             | AtComment
             | AtTerm
             deriving (Eq, Show, Generic, Typeable, Data, Store, ToJSON, FromJSON
                      , Bounded, Enum)
             deriving anyclass (NFData)
-- | Datatype for appliesto attribute
data AppliesTo = AtSource
               | AtTarget
               deriving (Eq, Show, Generic, Typeable, Data, Store, ToJSON, FromJSON
                        , Bounded, Enum)
               deriving anyclass (NFData)

-- | Datatype for elementtype attribute
data ElementType = EtFmt
                 | EtUI
                 | EtQuote
                 | EtLink
                 | EtImage
                 | EtOther
                 | EtCustom Text
                 deriving (Eq, Show, Generic, Typeable, Data, Store, ToJSON, FromJSON)
                 deriving anyclass (NFData)

-- | Datatype for reorder attribute
data Reorder = ROYes
             | RONo
             | ROFirstNo
             deriving (Eq, Show, Generic, Typeable, Data, Store, ToJSON, FromJSON
                      , Bounded, Enum)
             deriving anyclass (NFData)

-- | Datatype for segment state attribute
data SegState = SsInitial
              | SsTranslated
              | SsReviewed
              | SsFinal
              deriving (Eq, Show, Generic, Typeable, Data, Store, ToJSON, FromJSON
                       , Bounded, Enum)
              deriving anyclass (NFData)

-- | Datatype for sub type attribute
data SubType = XlfLb
             | XlfPb
             | XlfB
             | XlfI
             | XlfU
             | XlfVar
             | STCustom Text
             deriving (Eq, Show, Generic, Typeable, Data, Store, ToJSON, FromJSON)
             deriving anyclass (NFData)

-- | Datatype for text direction attribute
data TextDir = TdLtR
             | TdRtL
             | TdAuto
             deriving (Eq, Show, Generic, Typeable, Data, Store, Bounded, Enum)
             deriving anyclass (NFData)

------------------------------------------------------------------------------------------
--  Instances
------------------------------------------------------------------------------------------


instance ShowAttr (IsDef v) where showAttr = showAttr . snd

instance ReadAttr (Either AllLangs Lang)  where
  readAttr = \case "*all*" -> Just $ Left AllLangs
                   l       -> Right <$> readAttr l
instance ShowAttr (Either AllLangs Lang)  where
  showAttr (Left  AllLangs) = "*all*"
  showAttr (Right l)        = showAttr l

instance ReadAttr SegType where readAttr = convertType showAttr
instance ShowAttr SegType where
  showAttr = \case STBlock  -> "block"
                   STPara   -> "paragraph"
                   STSent   -> "sentence"
                   STPhrase -> "phrase"

instance ReadAttr DataType where
  readAttr dt = case toLower dt of
    "unknown"    -> Just DTUnknown
    "alptext"    -> Just DTAlpText
    "cdf"        -> Just DTCdf
    "cmx"        -> Just DTCmx
    "cpp"        -> Just DTCpp
    "hptag"      -> Just DTHpTag
    "html"       -> Just DTHtml
    "interleaf"  -> Just DTInterleaf
    "ipf"        -> Just DTIpf
    "java"       -> Just DTJava
    "javascript" -> Just DTJavascript
    "lisp"       -> Just DTLisp
    "mif"        -> Just DTMif
    "opentag"    -> Just DTOpenTag
    "pascal"     -> Just DTPascal
    "plaintext"  -> Just DTPlainText
    "pm"         -> Just DTPm
    "rtf"        -> Just DTRtf
    "sgml"       -> Just DTSgml
    "stf-f"      -> Just DTStfF
    "stf-i"      -> Just DTStfI
    "transit"    -> Just DTTransit
    "vbscript"   -> Just DTVbSript
    "winres"     -> Just DTWinRes
    "xml"        -> Just DTXml
    "xptag"      -> Just DTXpTag
    "text"       -> Just DTText
    t            -> Just $ DTCustom t

instance ShowAttr DataType where
  showAttr = \case DTUnknown    -> "unknown"
                   DTAlpText    -> "alptext"
                   DTCdf        -> "cdf"
                   DTCmx        -> "cmx"
                   DTCpp        -> "cpp"
                   DTHpTag      -> "hptag"
                   DTHtml       -> "html"
                   DTInterleaf  -> "interleaf"
                   DTIpf        -> "ipf"
                   DTJava       -> "java"
                   DTJavascript -> "javascript"
                   DTLisp       -> "lisp"
                   DTMif        -> "mif"
                   DTOpenTag    -> "opentag"
                   DTPascal     -> "pascal"
                   DTPlainText  -> "plaintext"
                   DTPm         -> "pm"
                   DTRtf        -> "rtf"
                   DTSgml       -> "sgml"
                   DTStfF       -> "stf-f"
                   DTStfI       -> "stf-i"
                   DTTransit    -> "transit"
                   DTVbSript    -> "vbscript"
                   DTWinRes     -> "winres"
                   DTXml        -> "xml"
                   DTXpTag      -> "xptag"
                   DTText       -> "text"
                   DTCustom t   -> t

instance ReadAttr PropType where
  readAttr dt = case toLower dt of
    -- bpt & it
    "bold"    -> Just PTBold
    "color"   -> Just PTColor
    "dulined" -> Just PTDuLined
    "font"    -> Just PTFont
    "italic"  -> Just PTItalic
    "link"    -> Just PTLink
    "scap"    -> Just PTScap
    "struct"  -> Just PTStruct
    "ulined"  -> Just PTULined
    -- ph
    "index"   -> Just PTIndex
    "date"    -> Just PTDate
    "time"    -> Just PTTime
    "fnote"   -> Just PTFNote
    "enote"   -> Just PTENote
    "alt"     -> Just PTAlt
    "image"   -> Just PTImage
    "pb"      -> Just PTPb
    "lb"      -> Just PTLb
    "cb"      -> Just PTCb
    "inset"   -> Just PTInSet
    t         -> Just $ PTCustom t
instance ShowAttr PropType where
  showAttr = \case
    -- bpt & it
    PTBold     -> "bold"
    PTColor    -> "color"
    PTDuLined  -> "dulined"
    PTFont     -> "font"
    PTItalic   -> "italic"
    PTLink     -> "link"
    PTScap     -> "scap"
    PTStruct   -> "struct"
    PTULined   -> "ulined"
 -- ph
    PTIndex    -> "index"
    PTDate     -> "date"
    PTTime     -> "time"
    PTFNote    -> "fnote"
    PTENote    -> "enote"
    PTAlt      -> "alt"
    PTImage    -> "image"
    PTPb       -> "pb"
    PTLb       -> "lb"
    PTCb       -> "cb"
    PTInSet    -> "inset"
    PTCustom t -> t

instance ReadAttr PhAssoc where readAttr = convertType showAttr
instance ShowAttr PhAssoc where
  showAttr = \case PHAPrev      -> "p"
                   PHAFollowing -> "f"
                   PHABoth      -> "b"
