-- | Module      : Tmx.Parser.Parser
--   Description : Parsers and checkers for each tag of TMX spec
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Tmx.Parser.Tags
  ( parseTmx

  , semanticUde
  , semanticTu
  , semanticInterSegs
  , semanticContent

  , mkTmx
  , mkHeaderTag
  , mkUdeTag
  , mkNotes
  , mkProps
  , mkTransUnitTag
  , mkTransUnitVarTag

  , TmxState
  , module X
  )
where

import Imports

import Text.XML.SAXCombinators
import Tmx.Parser.Attributes          as X
import Tmx.Parser.Content
import Tmx.Errors.Errors       as X
import Tmx.Types               as X
import Tmx.Utils


-- | Parser state holding 'Global' vars and user supplied state
type TmxState a = (Globals, a)

------------------------------------------------------------------------------------------
-- Syntax checks
------------------------------------------------------------------------------------------

-- | Parse events as 'TmxCore' and only its 'HeaderCore' as children
--   Does not parse <body> and its children
--   Returns also list of unknown 'Node's in <tmx>, in <header> and unknown 'Node's
--   before <body>
mkTmx :: TmxParser m
      => Text -> UniqTmxId -> EventL -> [EventL] -> m (TmxCore, [IdxNode],Globals)
mkTmx title uniqId e (clearBlanksE -> es) = do
  let (cs,_rest) = spanUntilTagE "body" es
  toElement (e,cs) >>= \case
    (n@(_,NElement _ as _ cs'), []) -> do
      (hdr,oth,g) <- parseTag n "header" mkHeaderTag cs'
      tmx <- TmxCore title uniqId <$> checkVer n as <*> pure hdr
      pure (tmx,oth,g)
    _ -> throwError $ WrongTag "tmx" (Left "tmx")
  where
    checkVer n as = reqAttr n "version" as >>= \case
      "1.4"  -> pure "1.4"
      v      -> mkErr n $ BadVersion (v, "1.4")

-- | Parse events as 'HeaderCore'. Returns also list of unknown 'Node's
mkHeaderTag :: TmxParser m => IdxNode -> m (HeaderCore, [IdxNode], Globals)
mkHeaderTag n@(_,NElement "header" as _ (clearBlanksN -> cs)) = do
  let (ps,ns,us,unk) = splitPropNote ["ude"] cs
  g@Globals{..} <- Globals
    <$>          reqAttrRead     n       "srclang"    as  -- glbSrcLang
    <*>          parseChangeInfo n                    as  -- glbChangeInfo
    <*>          reqAttrRead     n       "datatype"   as  -- glbDataType
    <*>          reqAttrRead     n       "segtype"    as  -- glbSegType
    <*>          reqAttr         n       "o-tmf"      as  -- glbOrigFormat
    <*> (fst <$> defAttr         n utf16 "o-encoding" as) -- glbOEncoding

  (,unk,g) <$> (HeaderCore glbSrcLang glbChngInfo glbDataType glbSegType glbOEncoding
                  <$> reqAttrRead     n       "adminlang"  as -- hdrAdminLang
                  <*> defAttr         n utf16 "o-encoding" as -- hdrOEncoding
                  <*> mkProps g ps                            -- hdrProp
                  <*> mkNotes g ns                            -- hdrNote
                  <*> parseTagMay "ude" mkUdeTag us)          -- hdrUde
mkHeaderTag n = mkErr n $ WrongTag "header"

-- | Parse events as 'UdeCore'
mkUdeTag :: TmxParser m => IdxNode -> m UdeCore
mkUdeTag n@(_,NElement "ude" as _ (clearBlanksN -> cs)) = do
  nonEmpty <$> mapM mkMapTag (filter (isNamed "map" . snd) cs) >>= \case
    Nothing -> mkErr n $ MustHaveOne ("ude","map")
    Just m  -> UdeCore <$> reqAttr     n "name" as -- udeName
                       <*> optAttrRead n "base" as -- udeBase
                       <*> pure m                  -- udeMaps

  where
    mkMapTag m@(_, NElement "map" mas _ []) =
      MapCore <$> (hexToInt    m =<< reqAttr n "unicode" mas) -- mapUnicode
              <*> (optHexToInt m =<< optAttr   "code"    mas) -- mapCode
              <*>  optAttr                     "ent"     mas  -- mapEnt
              <*>  optAttr                     "subst"   mas  -- mapSubst
    mkMapTag m = mkErr m $ ExtraTag ("ude","map")
mkUdeTag n = mkErr n $ WrongTag "ude"

-- | Parse events as a list of 'NoteCore's
mkNotes :: TmxParser m => Globals -> [IdxNode] -> m [NoteCore]
mkNotes Globals{..} (clearBlanksN -> cs) =
  mapM mkNote . zip [1::Int ..] $ filter (isNamed "note" . snd) cs
  where
    mkNote (order, n@(_,NElement _ as _ ts)) =
      NoteCore order <$> defAttr n glbSrcLang   "xml:lang"   as -- noteXmlLang
                     <*> defAttr n glbOEncoding "o-encoding" as -- noteOEncoding
                     <*> pure (fst $ spanConcatTextN "" ts)     -- noteText
    mkNote (_,n) = mkErr n $ WrongTag "note"

-- | Parse events as a list of 'PropCore's
mkProps :: TmxParser m => Globals -> [IdxNode] -> m [PropCore]
mkProps Globals{..} (clearBlanksN -> cs) = mapM mkProp $ filter (isNamed "prop" . snd) cs
  where
    mkProp n@(_,NElement _ as _ ps) =
      PropCore <$> reqAttrRead  n              "type"       as -- propType
               <*> defAttr      n glbSrcLang   "xml:lang"   as -- propXmlLang
               <*> defAttr      n glbOEncoding "o-encoding" as -- propOEncoding
               <*> pure (fst $ spanConcatTextN "*" ps)         -- propText
    mkProp n = mkErr n $ WrongTag "prop"

-- | Parse events as 'TransUnitCore' and all its variants ('TransUnitVarCore')
--   Returns also list of unknown 'Node's
mkTransUnitTag :: TmxParser m => Globals -> IdxNode -> m (TransUnitCore,[IdxNode])
mkTransUnitTag g@Globals{..} n@(order,NElement "tu" as _ (clearBlanksN -> cs)) = do
  let tUid           = "###-" <> tshow order <> "-###"
      (ps,ns,us,unk) = splitPropNote ["tuv"] cs
  nonEmpty <$> mapM (mkTransUnitVarTag g) (filter (isNamed "tuv" . snd) us) >>= \case
    Nothing -> mkErr n $ MustHaveOne ("ude","map")
    Just tuvs  ->
      (,unk) <$> (TransUnitCore
        <$>                  defAttr         n tUid          "tuid"       as  -- tuId
        <*> pure order                                                        -- tuOrder
        <*>                  defAttr         n glbSrcLang    "srclang"    as  -- tuSrcLang
        <*>                  parseChangeInfo n                            as  -- tuChangeInfo
        <*>                  defAttr         n glbDataType   "datatype"   as  -- tuDataType
        <*>                  defAttr         n glbSegType    "segtype"    as  -- tuSegType
        <*>                  defAttr         n glbOrigFormat "o-tmf"      as  -- tuOrigFormat
        <*>                  defAttr         n glbOEncoding  "o-encoding" as  -- tuOEncoding
        <*> (fromMaybe 0 <$> optAttrRead     n               "usagecount" as) -- tuUsageCount
        <*> mkProps g ps                                                      -- tuProp
        <*> mkNotes g ns                                                      -- tuNote
        <*> pure tuvs)                                                        -- tuTUVs
mkTransUnitTag _ n = mkErr n $ WrongTag "tu"

-- | Parse events as 'TransUnitVarCore'
--   Ignores unknown 'Node's
mkTransUnitVarTag :: TmxParser m => Globals -> IdxNode -> m TransUnitVarCore
mkTransUnitVarTag g@Globals{..} n@(_,NElement "tuv" as _ (clearBlanksN -> cs)) =
  TransUnitVarCore
    <$>                  reqAttrRead  n               "xml:lang"   as  -- tuvXmlLang
    <*> parseChangeInfo n                                          as  -- tuvChangeInfo
    <*>                  defAttr      n glbDataType   "datatype"   as  -- tuvDataType
    <*>                  defAttr      n glbOrigFormat "o-tmf"      as  -- tuvOrigFormat
    <*>                  defAttr      n glbOEncoding  "o-encoding" as  -- tuvOEncoding
    <*> (fromMaybe 0 <$> optAttrRead  n               "usagecount" as) -- tuvUsageCount
    <*> mkProps g cs                                                   -- tuvProp
    <*> mkNotes g cs                                                   -- tuvNote
    <*> parseTag n "seg" mkSegTag cs                                   -- tuvSeg

  where
    mkSegTag (_idx,NElement "seg" _ _ (clearBlanksN -> ss)) = mapM mkContentTag ss
    mkSegTag n' = mkErr n' $ WrongTag "seg"
mkTransUnitVarTag _ n = mkErr n $ WrongTag "tu"



------------------------------------------------------------------------------------------
-- Semantic checks
------------------------------------------------------------------------------------------

-- | Check semantic validity of <ude> tag
semanticUde :: TmxParser m => UdeCore -> m UdeCore
semanticUde u@UdeCore{..} = mapM_ go udeMaps >> return u
  where
    go MapCore{..} = do
      when (null $ catMaybes [tshow <$> mapCode, mapEnt, mapSubst]) $
        semanticErr u $ MustHaveOne ("map","'code', 'ent' or 'subst'")
      when (isJust mapCode && isNothing udeBase) $
        semanticErr u IfCodeMapUdeBase

-- | Check semantic validity of <seg> tag, but only on one seg level
semanticContent :: TmxParser m => Content -> m Content
semanticContent cs = fst <$> foldlM checkBptEpt ([],mempty :: Map Int Bpt) cs
  where
    checkBptEpt (xs,m) c = case c of
      (_,BPT b@Bpt{..}) -> pure (xs <> [c], insertMap bptIdx b m)
      (_,EPT   Ept{..}) -> do
        when (isNothing $ lookup eptIdx m) $ semanticErr ("Content"::Text) EptBeforeBpt
        pure (xs <> [c], m)
      _ -> pure (xs <> [c], m)

-- | Check semantic validity of <tu> tag, semantic validity of all its <tuv> variants
---  and inter-segment validity
semanticTu :: TmxParser m => TransUnitCore -> m TransUnitCore
semanticTu tu@TransUnitCore{..} = do
  let (src :| trgs) = tuTUVs
  void $ semanticInterSegs (tuvContent src, map tuvContent trgs)
  pure tu

-- | Check semantic validity of main Source variant and its Target translations
semanticInterSegs :: TmxParser m => (Content,[Content]) -> m (Content,[Content])
semanticInterSegs (src,trgs) = do
  void $ semanticContent src
  mapM_ semanticContent trgs
  let srcSets = foldl' mkSets (intSet,intSet,intSet,intSet,intSet) src
  mapM_ (mapM_ (checkCodes srcSets)) trgs
  pure (src,trgs)
  where
    checkCodes (bs, es, is, ps, hs) c = case snd c of
      BPT b -> unless (bptExternal b `existsIn` bs) (err "bpt") >> pure ()
      EPT e -> unless (eptExternal e `existsIn` es) (err "ept") >> pure ()
      IT  i -> unless (itExternal  i `existsIn` is) (err "it")  >> pure ()
      PH  p -> unless (phExternal  p `existsIn` ps) (err "ph")  >> pure ()
      HI  h -> unless (hiExternal  h `existsIn` hs) (err "hi")  >> pure ()
      _     ->                                                     pure ()
      where
        err t = semanticErr ("External attribute"::Text) $ InlCodeMissingInVariant t
        existsIn x xs = maybe True (`member` xs) x
    mkSets (bs, es, is, ps, hs) c = case c of
      (_,BPT b) -> (maybe bs (`insertSet` bs) $ bptExternal b, es, is, ps, hs)
      (_,EPT e) -> (bs, maybe es (`insertSet` es) $ eptExternal e, is, ps, hs)
      (_,IT  i) -> (bs, es, maybe is (`insertSet` is) $ itExternal i,  ps, hs)
      (_,PH  p) -> (bs, es, is, maybe ps (`insertSet` ps) $ phExternal p,  hs)
      (_,HI  h) -> (bs, es, is, ps, maybe hs (`insertSet` hs) $ hiExternal h)
      _         -> (bs, es, is, ps, hs)
    intSet = mempty :: IntSet


-- | Parse SAX events as TmxCore tag and sequence of <tu> tags.
--   Returns TmxTag and unknown tags
parseTmx :: Text -> UniqTmxId -> (EventL, [EventL], TmxState a)
         -> Either ((Tmx,[IdxNode]), TmxState a) ([(Tmx,[IdxNode])], [EventL], TmxState a)
parseTmx title uId (e@(_idx,TagStart nm _as, _), es, s) =
  case join . tryError $ go s of
    Right (Just x) -> Right x
    Right Nothing  -> Right ([], es, s)
    Left  err      -> Left ((TmxError $ CustomError (tshow err) (Left ""),[]), s)

  where
    go :: (Globals, s) -> Either PError (Maybe ([(Tmx,[IdxNode])], [EventL], (Globals, s)))
    go (g,c) = case nm of
      "tu" -> do
        (n, rs) <- toElement (e, es)
        (tu,unk) <- mkTransUnitTag g n
        pure . Just $ ([(TmxTransUnit tu,unk)], rs, (g,c))
      "tmx" -> do
        let (cs,rest) = spanUntilTagE "body" $ clearBlanksE es
        (tmx, unk, g') <- mkTmx title uId e cs
        pure $ Just ([(TmxTmx tmx,unk)], rest, (g',c))
      "body" -> pure Nothing
      _      -> pure $ Just ([(TmxUnknownEvent e,[])], es, (g,c))
parseTmx _ _ ((_,TagEnd "tu", _),   es, s) = Right ([], es, s)
parseTmx _ _ ((_,TagEnd "tmx", _),  es, s) = Right ([], es, s)
parseTmx _ _ ((_,TagEnd "body", _), es, s) = Right ([], es, s)
parseTmx _ _ ((_,XmlDeclaration ver enc b,_), es, s) =
  let t = ver <> maybe "" (" <> " <>) enc <> " <> " <> tshow b
   in Right ([(TmxDecl t,[])], es, s)
parseTmx _ _ (e, es, s) = Right ([(TmxUnknownEvent e,[])], es, s)


------------------------------------------------------------------------------------------
-- Utils
------------------------------------------------------------------------------------------

-- | Split 'Node's into four categories, 'PropCore', 'Note', specified tags, and others
splitPropNote :: [Text] -> [IdxNode] -> ([IdxNode], [IdxNode], [IdxNode], [IdxNode])
splitPropNote used = foldr go ([],[],[],[])
  where
    go n (ps,ns,us,oth) = case getName $ snd n of
      "prop" ->                    (n:ps,  ns,  us,  oth)
      "note" ->                    (  ps,n:ns,  us,  oth)
      nm -> if nm `elem` used then (  ps,  ns,n:us,  oth)
                              else (  ps,  ns,  us,n:oth)
