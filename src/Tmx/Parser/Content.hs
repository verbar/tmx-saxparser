-- | Module      : Tmx.Parser.Content
--   Description : Parser for tags with translation content
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Tmx.Parser.Content
  ( mkContentTag
  , toHtml
  , replaceTags
  )
where

import Imports
import HTMLEntities.Text

import Text.XML.SAXCombinators
import Tmx.Errors.Errors
import Tmx.Types
import Tmx.Utils

-- import Import.Debug

-- | Parse TU content. unknown tags not allowed
mkContentTag :: TmxParser m => IdxNode -> m (Int,ContentTag)
mkContentTag n@(idx, NElement nm _ _ _) = case nm of
  "ph"  -> (idx,) . PH  <$> parsePh  n
  "sub" -> (idx,) . SUB <$> parseSub n
  "hi"  -> (idx,) . HI  <$> parseHi  n
  "it"  -> (idx,) . IT  <$> parseIt  n
  "bpt" -> (idx,) . BPT <$> parseBpt n
  "ept" -> (idx,) . EPT <$> parseEpt n
  _     -> mkErr n . UnknownElement $ tshow n

mkContentTag (idx,NText  t)   = pure (idx, TEXT t)
mkContentTag (idx,NCData t)   = pure (idx, TEXT t)
mkContentTag (idx,NComment t) = pure (idx, COMM t)
-- mkContentTag (idx,NProcInstruction a b) = pure $ (idx, CtC a b)
mkContentTag n                = mkErr n . WrongTag $ tshow n

-- | Parse 'Bpt' tag and its children
parseBpt :: TmxParser m => IdxNode -> m Bpt
parseBpt n@(_,NElement "bpt" as _ cs) =
  Bpt <$> reqAttrRead n "i"     as -- bptIdx
      <*> optAttrRead n "x"     as -- bptExternal
      <*> optAttrRead n "type"  as -- bptType
      <*> parseChildren cs         -- bptContent
parseBpt n = mkErr n . WrongTag $ tshow n

-- | Parse 'Ept' tag and its children
parseEpt :: TmxParser m => IdxNode -> m Ept
parseEpt n@(_,NElement "ept" as _ cs) =
  Ept <$> reqAttrRead n "i" as -- eptIdx
      <*> pure Nothing
      <*> parseChildren cs     -- eptContent
parseEpt n = mkErr n . WrongTag $ tshow n

-- | Parse 'Sub' tag
parseSub :: TmxParser m => IdxNode -> m Sub
parseSub n@(_,NElement "sub" as _ cs) =
  Sub <$> optAttrRead n "datatype" as -- subDataType
      <*> optAttrRead n "type"     as -- subType
      <*> mapM mkContentTag cs        -- subContent
parseSub n = mkErr n . WrongTag $ tshow n

-- | Parse 'Ph' tag and its children
parsePh :: TmxParser m => IdxNode -> m Ph
parsePh n@(_,NElement "ph" as _ cs) =
  Ph <$> optAttrRead n "x"     as -- phExternal
     <*> optAttrRead n "type"  as -- phType
     <*> optAttrRead n "assoc" as -- phAssoc
     <*> parseChildren cs      -- phContent
parsePh n = mkErr n . WrongTag $ tshow n

-- | Parse 'Hi' tag and its children
parseHi :: TmxParser m => IdxNode -> m Hi
parseHi n@(_,NElement "hi" as _ cs) =
  Hi <$> optAttrRead n "x"     as -- hiExternal
     <*> optAttrRead n "type"  as -- hiType
     <*> parseChildren cs         -- hiContent
parseHi n = mkErr n . WrongTag $ tshow n

-- | Parse 'It' tag and its children
parseIt :: TmxParser m => IdxNode -> m It
parseIt n@(_,NElement "It" as _ cs) =
  It <$> reqAttrRead n "pos"   as -- itIsolated
     <*> optAttrRead n "x"     as -- itExternal
     <*> optAttrRead n "type"  as -- itType
     <*> parseChildren cs         -- itContent
parseIt n = mkErr n . WrongTag $ tshow n

-- | Collect all text content and dive in if 'Sub' tag encountered
parseChildren :: TmxParser m => [IdxNode] -> m [(Int,ContentTag)]
parseChildren = mapM f
  where
    f (idx,n) = (idx,) <$> if isNamed "sub" n
                             then        SUB <$> parseSub (idx,n)
                             else pure . TEXT $  getText  n


-- | Convert TU 'Content' to text. Each tag will be replaced by a
--   <span> tag with specific attributes.
toHtml :: Content -> (Text, Content)
toHtml = convert False
  where
    convert isInner        = foldr (go isInner) ("",[])
    go isInner (idx, tag) (txt, xs) = do
      let replaceTag nm cont =
            let (txtContent,tagsContent) = convert True cont
             in ( tagToText isInner idx nm txtContent <> txt
                , (idx, tag) : tagsContent <> xs)
      case tag of
        TEXT t        -> (t <> txt, xs)
        COMM _        -> (txt, xs)
        BPT  Bpt {..} -> replaceTag "bpt" bptContent
        EPT  Ept {..} -> replaceTag "ept" eptContent
        IT   It  {..} -> replaceTag "it"  itContent
        PH   Ph  {..} -> replaceTag "ph"  phContent
        HI   Hi  {..} -> replaceTag "hi"  hiContent
        SUB  Sub {..} -> replaceTag "sub" subContent
    tagToText (bool "\"" "'" -> quote) i nm t = do
      let
          idx   = tshow i
      "<span class='tmx-tag tmx-"  <> nm  <> "'"
        <> " id='"   <> idx <> "'"
        <> " name='" <> idx <> "'"
        <> " title='"  <> nm <> ": " <> text t <> "'"
        <> if null t
             then ""
             else " data-content=" <> quote <> t <> quote
        <> ">"
        <> "<" <> idx <> ">"
        <> "</span>"


-- | Convert TU 'Content' to text. Each opening and closing tag will be replaced with
--   text provided supplied function.
-- /Meaning/:
--   Bool param for function signifies if tag is CLOSE tag.
replaceTags :: (Bool -> Int -> Text) -> Content -> (Text, Content)
replaceTags replTag = foldr go ("",[])
  where
    go (idx, tag) (txt, xs) = do
      let openTag  = replTag False idx
          closeTag = replTag True  idx
          replaceTag cont =
            let (txtContent,tagsContent) = replaceTags replTag cont
             in ( openTag <> txtContent <> closeTag <> txt
                , (idx, tag) : tagsContent <> xs)
      case tag of
        TEXT t        -> (t <> txt, xs)
        COMM _        -> (txt, xs)
        BPT  Bpt {..} -> replaceTag bptContent
        EPT  Ept {..} -> replaceTag eptContent
        IT   It  {..} -> replaceTag itContent
        PH   Ph  {..} -> replaceTag phContent
        HI   Hi  {..} -> replaceTag hiContent
        SUB  Sub {..} -> replaceTag subContent


-- |
-- This is
--   <bpt><a href='http://localhost title='<sub>Inside BPT</sub>'</bpt>
--   in the middle
--   <sub>Go to Notes</sub>
--   <ept></a></ept>
--  .
-- This is
--   <2><a href='http://localhost' title='<104>Inside BPT</104>'</2>
--   in the middle
--   <4>Go to Notes</4>
--   <5></a></5>
-- .


-- f1 :: IO ()
-- f1 = do
--   let replTag isEnd i = "<" <> bool "" "/" isEnd <> tshow i <> ">"
--       -- (t1, _xs1) = toHtml testContent
--       (t2, _xs2) = replaceTags replTag testContent
--   -- putStrLn t1
--   putStrLn t2
--   mapM_ print _xs2

-- -- | See the

-- testContent :: Content
-- testContent = zip [1..] $
--   [ TEXT "This is "
--   , BPT Bpt
--     { bptIdx      = 2
--     , bptExternal = Nothing
--     , bptType     = Nothing
--     , bptContent  =
--       [ (101, TEXT "<a ")
--       , (102, TEXT "href='http://localhost' ")
--       , (103, TEXT "title='")
--       , (104, SUB Sub
--           { subDataType = Nothing
--           , subType     = Nothing
--           , subContent  = [ (105,TEXT "Inside BPT") ]
--           }
--         )
--       , (107, TEXT "'")
--       ]
--     }
--   , TEXT " in the middle "
--   , SUB Sub   { subDataType = Nothing
--               , subType     = Nothing
--               , subContent  = [(200,TEXT "Go to Notes")]
--               }
--   , EPT Ept { eptIdx      = 5
--             , eptExternal = Nothing
--             , eptContent  = [(300, TEXT "</a>")]
--             }
--   , COMM "Some content"

--     -- | IT   It
--     -- | PH   Ph
--     -- | HI   Hi
--     -- | SUB  Sub
--   , TEXT "."
--   ]
