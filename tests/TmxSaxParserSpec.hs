-- | Module      : SaxCombinatorsSpec
--   Description : Combinators for lazy stream parsing with SAX events
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module TmxSaxParserSpec
  -- (main, spec)
where

import           ClassyPrelude


import qualified Data.ByteString.Lazy           as BL
import           Data.List.NonEmpty             (NonEmpty (..))
import           Data.Time.Clock                (DiffTime)
import           Text.XML.Expat.SAX             (SAXEvent (..), XMLParseLocation,
                                                 defaultParseOptions, parseLocations)
import           Text.XML.SAXCombinators
import           Text.XML.SAXCombinators.Hexpat ()


import           Test.Hspec

import           Tmx.Parser
import           Tmx.Parser.Tags                (mkHeaderTag, mkNotes, mkProps,
                                                 mkTransUnitTag, mkTransUnitVarTag,
                                                 mkUdeTag, parseTmx, semanticTu,
                                                 semanticUde)
-- import Import.Debug


main :: IO ()
main = hspec spec


pl :: Int64 -> Int64 -> Int64 -> Either a ParseLocation
pl l c bc = Right $ ParseLocation { plLine      = l
                                  , plColumn    = c
                                  , plByteIndex = 0
                                  , plByteCount = bc
                                  }

spec :: Spec
spec = do
  describe "Parser can parse" $ do
    it "<ude> tag without <map>" $ do
      mkUdeTag (fst $ asElement udeTwoMap) `shouldBe`
        Left (MustHaveOne ("ude","map") (pl 1 0 38))

    it "<ude> tag with <map>" $ do
      mkUdeTag (fst $ asElement udeTwMap) `shouldBe` Right udeWTag

    it "<note> tag" $ do
      mkNotes wrkGlobals [fst $ asElement noteT1] `shouldBe` Right [note1Tag]

    it "<prop> tag" $ do
      mkProps wrkGlobals (map (fst . asElement) [propT1, propT2]) `shouldBe`
        Right [prop1Tag, prop2Tag]

    it "<header> tag" $ do
      (mkHeaderTag . fst . asElement $ hdrT <> endTag "header") `shouldBe`
        Right (headerTag, [], Globals { glbSrcLang      = Right en
                                      , glbChngInfo     = tmxChangeInfo
                                      , glbDataType     = DTPlainText
                                      , glbSegType      = STSent
                                      , glbOrigFormat   = "ABCTransMem"
                                      , glbOEncoding    = "iso-8859-1"}
              )

    it "<tuv> tag (src1)" $
      mkTransUnitVarTag wrkGlobals (fst $ asElement tuvSrc1) `shouldBe` Right srcLongTag

    it "<tuv> tag (trgLong)" $
      mkTransUnitVarTag wrkGlobals (fst . asElement $ tuvTrgLong False) `shouldBe`
        Right tuvTrgLongTag

    it "<tu> tag (tuT2)" $
      mkTransUnitTag wrkGlobals (fst $ asElement tuT2) `shouldBe` Right (tuT2Tag, [])

    it "<tmx> tag (tmxTag) and capture unknown tags" $ do
      let (tmxOpenEvent,  es) = splitHeadTail . asEvents . asSAX $ tmxT False
      case parseTmx "title" 1 (tmxOpenEvent, es, (wrkGlobals, ())) of
        Left  (e,_)              -> error $ show e
        Right ([(tmx,unks)],_,_) -> do
          tmx `shouldBe` TmxTmx tmxMainTag
          unks `shouldBe`
            [(13,NElement "markec" [] (ParseLocation 1 542 542 8)
                                     [(14,NText "Some markec text")])]
        Right _ -> error "No TMX or more for some reason. Shouldn't happen!!!"

    it "<tmx> tag (tmxTag) and captures error" $ do
      let (tmxOpenEvent,  es) = splitHeadTail . asEvents . asSAX $ tmxT True
      case parseTmx "title" 1 (tmxOpenEvent, es, (wrkGlobals, ())) of
        Left  (e,_)              -> error $ show e
        Right ([(tmx,unks)],_,_) -> do
          tmx  `shouldBe` TmxTmx tmxMainTag
          unks `shouldBe` [(13,NFail "\"mismatched tag\"" (ParseLocation 1 544 544 0))]
        Right _ -> error "No TMX or more for some reason. Shouldn't happen!!!"

  describe "<tuv> conversion:" $ do
    it "replace tags as <num>" $ do
      let TransUnitVarCore{..} =
            case mkTransUnitVarTag wrkGlobals (fst $ asElement tuvSrcLong) of
              Right r -> r
              Left e  -> error $ show e
          replTag isEnd i = "<" <> bool "" "/" isEnd <> tshow i <> ">"
      replaceTags replTag tuvContent `shouldBe`
        ( concat [ "See the <5><a title=\""
                 , "<8>Go to Notes</8>\""
                 , " href=\"notes.htm\"></5>"
                 , "Notes<15></A></15> for more details."
                 ]
        , srcLongContentTags
        )

    it "to HTML" $ do
      let TransUnitVarCore{..} =
            case mkTransUnitVarTag wrkGlobals (fst $ asElement tuvSrcLong) of
              Right r -> r
              Left e  -> error $ show e
      toHtml tuvContent `shouldBe`
        ( concat
            [ "See the <span class='tmx-tag tmx-bpt' id='5' name='5' title='bpt: &lt;"
            , "a title=&quot;&lt;span class=&#39;tmx-tag tmx-sub&#39; id=&#39;8&#39; "
            , "name=&#39;8&#39; title=&#39;sub: Go to Notes&#39; data-content="
            , "&#39;Go to Notes&#39;&gt;&lt;8&gt;&lt;/span&gt;&quot; "
            , "href=&quot;notes.htm&quot;&gt;' data-content=\"<a title=\""
            , "<span class='tmx-tag tmx-sub' id='8' name='8' "
            , "title='sub: Go to Notes' data-content='Go to Notes'><8></span>\" "
            , "href=\"notes.htm\">\"><5></span>Notes<span class='tmx-tag tmx-ept' id='15'"
            , " name='15' title='ept: &lt;/A&gt;' data-content=\"</A>\"><15></span> for "
            , "more details."
            ]
        , srcLongContentTags
        )

  describe "Semnantic checks:" $ do
    it "<ude> passed" $ do
      case mkUdeTag (fst $ asElement udeTwMap) of
        Left e    -> error $ show e
        Right ude -> semanticUde ude `shouldBe` Right ude
    it "<tu> passed" $ do
      case mkTransUnitTag wrkGlobals . fst $ asElement tuTLong of
        Left  e      -> error $ show e
        Right (tu,_) -> semanticTu tu `shouldBe` Right tu








-- | Conversion between types
asEvents :: [(SAXEvent Text Text, XMLParseLocation)] -> [EventL]
asEvents = map toIdxEventL . zip [1..]

asSAX :: BL.ByteString -> [(SAXEvent Text Text, XMLParseLocation)]
asSAX = parseLocations defaultParseOptions

asElement :: BL.ByteString -> (IdxNode, [EventL])
asElement bs =
  case toElement . splitHeadTail . asEvents $ asSAX bs of
    Left  e -> error e
    Right x -> x

-- | -------------------------------------------------------------------------------------
-- Data for testing
------------------------------------------------------------------------------------------

mapTx :: BL.ByteString
mapTx = "<map unicode=\"#xF8FF\" code=\"#xF0\" ent=\"Apple_logo\" subst=\"[Apple]\"/>"

mapT, udeTwoMap, udeTwMap, hdrT, unkT :: BL.ByteString
mapT      = "<map unicode=\"#xF8FF\" code=\"#xF0\" ent=\"Apple_logo\" subst=\"[Apple]\"/>"
udeTwoMap = "<ude name=\"MacRoman\" base=\"Macintosh\">"
udeTwMap  = udeTwoMap <> mapT <> endTag "ude"

unkT = "<markec>Some markec text</markec>"
hdrT = concat
  [ "<header creationtool=\"XYZTool\""
  , "        creationtoolversion=\"1.01-023\""
  , "        datatype=\"PlainText\""
  , "        segtype=\"sentence\""
  , "        adminlang=\"en-us\""
  , "        srclang=\"EN\""
  , "        o-tmf=\"ABCTransMem\""
  , "        creationdate=\"20020101T163812Z\""
  , "        creationid=\"ThomasJ\""
  , "        changedate=\"20020413T023401Z\""
  , "        changeid=\"Amity\""
  , "        o-encoding=\"iso-8859-1\""
  , ">"
  ]

noteT1, noteT2, propT1, propT2 :: BL.ByteString
noteT1 = "<note>Note 1</note>"
noteT2 = "<note>Note 2</note>"
propT1 = "<prop type=\"Prop1\">Prop value 1</prop>"
propT2 = "<prop type=\"Prop2\">Prop value 2</prop>"


tuvSrc1, tuvSrcLong, tuvTrg1, tuvTrg2, tuT1, tuT2, tuTLong :: BL.ByteString
tuvSrc1 = concat
  [ "<tuv xml:lang=\"EN\" creationdate=\"19970212T153400Z\" creationid=\"BobW\">"
  , "  <seg>data &#918; (with a non-standard character: &#xF8FF;).</seg>"
  , " </tuv>"
  ]
tuvTrg1 = concat
  [ "<tuv xml:lang=\"FR\" creationdate=\"19970212T153400Z\" creationid=\"BobW\">"
  , "  <seg>donn&#xE9;es (avec un caract&#xE8;re non standard: &#xF8FF;).</seg>"
  , "</tuv>"
  ]
tuvTrg2 = concat
  [ "<tuv xml:lang=\"HR\" creationdate=\"19970212T153400Z\" creationid=\"BobW\">"
  , "  <seg>podatak&#xE9;es (s nestandardnim znakom: &#xF8FF;).</seg>"
  , "</tuv>"
  ]

tuvSrcLong = concat
  [ "<tuv xml:lang=\"EN\">"
  , "  <seg>See the <bpt i=\"1\" x=\"2\" type=\"link\">&lt;a title=\"<sub>Go to Notes</sub>\" \
          \href=\"notes.htm\"></bpt>Notes<ept i=\"1\">&lt;/A></ept> for more details."
  , "  </seg>"
  , "</tuv>"
  ]

tuT1 = concat
  [ "<tu tuid=\"0001\" datatype=\"Text\" usagecount=\"2\" lastusagedate=\"19970314T023401Z\">"
  , "  <note>Text of a note at the TU level.</note>"
  , "  <prop type=\"x-Domain\">Computing</prop>"
  , "  <prop type=\"x-Project\">P&#x00E6;gasus</prop>"
  , tuvSrc1, tuvTrg1, tuvTrg2
  , endTag "tu"
  ]
tuT2 = concat
  [ "<tu tuid=\"0001\" datatype=\"Text\" usagecount=\"2\" lastusagedate=\"19970314T023401Z\">"
  , "  <note>Text of a note at the TU level.</note>"
  , "  <prop type=\"x-Domain\">Computing</prop>"
  , "  <prop type=\"x-Project\">P&#x00E6;gasus</prop>"
  , tuvSrcLong
  , endTag "tu"
  ]
tuTLong = concat
  [ "<tu tuid=\"0003\" srclang=\"*all*\">"
  ,   "<prop type=\"Domain\">Cooking</prop>"
  ,   tuvSrcLong
  ,   tuvTrgLong True
  , "</tu>"
  ]

tuvTrgLong :: Bool -> BL.ByteString
tuvTrgLong putX = concat
  [ "<tuv xml:lang=\"hr-HR\">"
  , "  <seg>Pogledajte <bpt i=\"1\" "
  , bool "" "x=\"2\" " putX
  , "type=\"link\">&lt;a title=\"<sub>Vidi bilješke</sub>\" \
           \href=\"notes.htm\"></bpt>bilješke<ept i=\"1\">&lt;/a></ept> za više pojedinosti."
  , "  </seg>"
  , "</tuv>"
  ]

tmxT :: Bool -> BL.ByteString
tmxT mkErr = concat
  [ "<tmx version=\"1.4\">"
  ,    hdrT
  ,      noteT1
  ,      propT1
  ,      udeTwMap
  ,      bool "" (endTag "ude") mkErr
  ,      unkT
  ,    endTag "header"
  ,    "<body>"
  ,      tuT1
  ,    endTag "body"
  ,  endTag "tmx"
  ]

tuT2Tag :: TransUnitCore
tuT2Tag = TransUnitCore
  { tuId         = ("0001",False)
  , tuOrder      = 1
  , tuSrcLang    = (Right enUS, True)
  , tuChngInfo   = emptyChangeInfo {ciLastUsed = Just $ mkUtc 1997 3 14 2 34 1}
  , tuDataType   = (DTText,False)
  , tuSegType    = (STSent, True)
  , tuOrigFormat =  ("",True)
  , tuOEncoding  =  (utf16,True)
  , tuUsageCount = 2
  , tuProps      = [ PropCore { propType      = PTCustom "x-domain"
                              , propXmlLang   = (Right enUS,True)
                              , propOEncoding = (utf16,True)
                              , propText      = "Computin*g"}
                   , PropCore { propType      = PTCustom "x-project"
                              , propXmlLang   = (Right enUS,True)
                              , propOEncoding = (utf16,True)
                              , propText      = "P*\230*gasus"}
                   ]
  , tuNotes      = [ NoteCore { noteOrder     = 1
                              , noteXmlLang   = (Right enUS,True)
                              , noteOEncoding = (utf16,True)
                              , noteText      = "Text of a note at the TU level."
                              }
                   ]
  , tuTUVs       = TransUnitVarCore
                   { tuvXmlLang    = Right en
                   , tuvChngInfo   = emptyChangeInfo
                   , tuvDataType   = (DTUnknown,True)
                   , tuvOrigFormat = ("",True)
                   , tuvOEncoding  = (utf16,True)
                   , tuvUsageCount = 0
                   , tuvProps      = []
                   , tuvNotes      = []
                   , tuvContent    =
                       [ (21,TEXT "See the ")
                       , (22,BPT (Bpt { bptIdx      = 1
                                      , bptExternal = Just 2
                                      , bptType     = Just PTLink
                                      , bptContent  =
                                          [ (23,TEXT "<")
                                          , (24,TEXT "a title=\"")
                                          , (25,SUB (Sub { subDataType = Nothing
                                                         , subType     = Nothing
                                                         , subContent  = [ (26,TEXT "Go to Notes")]
                                                         }
                                                    ))
                                          , (28,TEXT "\" href=\"notes.htm")
                                          , (29,TEXT "\">")
                                          ]}))
                       , (31,TEXT "Notes")
                       , (32,EPT (Ept { eptIdx      = 1
                                      , eptExternal = Nothing,
                                        eptContent  = [ (33,TEXT "<")
                                                      , (34,TEXT "/A>")
                                                      ]
                                      }
                                 )
                         )
                       , (36,TEXT " for more details.")
                       ]
                   } :| []
  }

srcLongTag :: TransUnitVarCore
srcLongTag = TransUnitVarCore
  { tuvXmlLang  = Right en
  , tuvChngInfo = emptyChangeInfo
                  { ciCreated   = Just $ mkUtc 1997 2 12 15 34 0
                  , ciCreatedBy = Just "BobW"
                  }
  , tuvDataType   = (DTUnknown,True)
  , tuvOrigFormat = ("",True)
  , tuvOEncoding  = (utf16,True)
  , tuvUsageCount = 0
  , tuvProps      = []
  , tuvNotes      = []
  , tuvContent    = [ (4,TEXT "data ")
                    , (5,TEXT "\918")
                    , (6,TEXT " (with a non-s")
                    , (7,TEXT "tandard character: ")
                    , (8,TEXT "\63743"),(9,TEXT ").")
                    ]
  }

tuvTrgLongTag :: TransUnitVarCore
tuvTrgLongTag = TransUnitVarCore
  { tuvXmlLang    = Right hrHR
  , tuvChngInfo   = emptyChangeInfo
  , tuvDataType   = (DTUnknown,True)
  , tuvOrigFormat = ("",True)
  , tuvOEncoding  = (utf16,True)
  , tuvUsageCount = 0
  , tuvProps      = []
  , tuvNotes      = []
  , tuvContent    =
      [ (4,TEXT "Pogledajte ")
      , (5,BPT (Bpt { bptIdx      = 1
                    , bptExternal = Nothing
                    , bptType     = Just PTLink
                    , bptContent =
                        [ (6,TEXT "<")
                        , (7,TEXT "a title=\"")
                        , (8,SUB (Sub { subDataType = Nothing
                                      , subType     = Nothing
                                      , subContent  = [ ( 9,TEXT "Vi")
                                                      , (10,TEXT "di biljeake")]
                                      }
                                 ))
                        , (12,TEXT "\" href=\"notes.htm\">")
                        ]}))
      , (14,TEXT "biljeake")
      , (15,EPT (Ept { eptIdx      = 1
                     , eptExternal = Nothing,
                       eptContent  = [ (16,TEXT "<")
                                     , (17,TEXT "/a>")
                                     ]
                     }
                )
        )
      , (19,TEXT " za viae pojedinosti.")
      ]
  }

udeWTag :: UdeCore
udeWTag = UdeCore
  { udeName = "MacRoman"
  , udeBase = Just "Macintosh"
  , udeMaps = MapCore { mapUnicode = 63743
                      , mapCode    = Just 240
                      , mapEnt     = Just "Apple_logo"
                      , mapSubst   = Just "[Apple]"
                      } :| []
  }

note1Tag :: NoteCore
note1Tag = NoteCore
  { noteOrder     = 1
  , noteXmlLang   = (Right enUS,True)
  , noteOEncoding = (utf16,True)
  , noteText      = "Note 1"
  }

prop1Tag, prop2Tag :: PropCore
prop1Tag = PropCore
  { propType      = PTCustom "prop1"
  , propXmlLang   = (Right enUS,True)
  , propOEncoding = (utf16,True)
  , propText      = "Prop value 1"
  }
prop2Tag = PropCore
  { propType      = PTCustom "prop2"
  , propXmlLang   = (Right enUS,True)
  , propOEncoding = (utf16,True)
  , propText      = "Prop value 2"
  }

headerTag :: HeaderCore
headerTag = HeaderCore
  { hdrSrcLang     = Right en
  , hdrChngInfo   = tmxChangeInfo
  , hdrDataType   = DTPlainText
  , hdrSegType    = STSent
  , hdrOrigFormat = "iso-8859-1"
  , hdrAdminLang  = enUS
  , hdrOEncoding  = ("iso-8859-1",False)
  , hdrProps      = []
  , hdrNotes      = []
  , hdrUde        = Nothing
  }

srcLongContentTags :: Content
srcLongContentTags =
  [ (5,BPT (Bpt { bptIdx      = 1
                , bptExternal = Just 2
                , bptType     = Just PTLink
                , bptContent  = [ (6,TEXT "<")
                                , (7,TEXT "a title=\"")
                                , (8,SUB (Sub { subDataType = Nothing
                                              , subType = Nothing
                                              , subContent = [ (9,TEXT "Go to Notes")]
                                              }
                                         )
                                  )
                                , (11,TEXT "\" href=\"notes.htm")
                                , (12,TEXT "\">")
                                ]
                }
           )
    )
  , (8,SUB (Sub { subDataType = Nothing
                , subType     = Nothing
                , subContent  = [(9,TEXT "Go to Notes")]
                }
           )
    )
  , (15,EPT (Ept { eptIdx      = 1
                 , eptExternal = Nothing
                 , eptContent  = [ (16,TEXT "<")
                                 , (17,TEXT "/A>")
                                 ]
                 }
            )
    )
  ]


tmxMainTag :: TmxCore
tmxMainTag = TmxCore
  { tmxTitle   = "title"
  , tmxUniqId  = 1
  , tmxVersion = "1.4"
  , tmxHeader  = HeaderCore
    { hdrSrcLang    = Right en
    , hdrChngInfo   = tmxChangeInfo
    , hdrDataType   = DTPlainText
    , hdrSegType    = STSent
    , hdrOrigFormat = "iso-8859-1"
    , hdrAdminLang  = enUS
    , hdrOEncoding  = ("iso-8859-1",False)
    , hdrProps      = [ prop1Tag { propOEncoding = ("iso-8859-1", True)
                                 , propXmlLang   = (Right en, True)
                                 }
                      ]
    , hdrNotes      = [ note1Tag{ noteOEncoding = ("iso-8859-1", True)
                                , noteXmlLang   = (Right en, True)
                                }
                      ]
    , hdrUde        = Just udeWTag
    }
  }


mkLang :: String -> String -> Lang
mkLang l r = Lang l "" r []

en, enUS, hrHR :: Lang
en = mkLang "en" ""
enUS = mkLang "en" "US"
hrHR = mkLang "hr" "HR"


wrkGlobals :: Globals
wrkGlobals = Globals (Right enUS) emptyChangeInfo DTUnknown STSent "" utf16

emptyChangeInfo :: ChangeInfo
emptyChangeInfo = ChangeInfo Nothing Nothing Nothing Nothing Nothing Nothing Nothing

tmxChangeInfo :: ChangeInfo
tmxChangeInfo = ChangeInfo
  { ciChanged       = Just $ mkUtc 2002 4 13  2 34 1
  , ciChangedBy     = Just "Amity"
  , ciCreated       = Just $ mkUtc 2002 1  1 16 38 12
  , ciCreatedBy     = Just "ThomasJ"
  , ciLastUsed      = Nothing
  , ciCreateTool    = Just "XYZTool"
  , ciCreateToolVer = Just "1.01-023"
  }

endTag :: BL.ByteString -> BL.ByteString
endTag t = "</" <> t <> ">"

fst3 :: (a,b,c) -> a
fst3 (x,_,_) = x

snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x

getEvs :: [EventL] -> [Event]
getEvs = map snd3

splitHeadTail :: [a] -> (a, [a])
splitHeadTail (x:xs) = (x,xs)
splitHeadTail [] = error "splitHeadTail: Empty list"

mkUtc :: Integer -> Int -> Int -> DiffTime -> DiffTime -> DiffTime -> UTCTime
mkUtc y mo d h mi s = UTCTime (fromGregorian y mo d) (h * 60 * 60 + mi * 60 + s)
